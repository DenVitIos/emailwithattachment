//
//  ViewController.swift
//  EmailAttachment
//
//  Created by Denis on 08.02.2020.
//  Copyright © 2020 Denis. All rights reserved.
//

import UIKit
import MessageUI

class AttachmentTableViewController: UITableViewController  {
    
    enum MIMEType: String {
        
        case html = "text/html"
        case jpg = "image/jpeg"
        case png = "image/png"
        case pdf = "application/pdf"
        
        init?(type: String) {
            switch type.lowercased() {
            case "html":
                self = .html
            case "jpg":
                self = .jpg
            case "png":
                self = .png
            case "pdf":
                self = .pdf
            default:
                return nil
            }
        }
    }
    let fileNamesArray = ["HtmlFile.html",
                          "JpegFile.jpg",
                          "PngFile.png",
                          "PdfFile.pdf"]
    let emailTitleString = "Email title"
    let messageBodyString = "Message body"
    let toRecipientsArray = ["denvitios@gmail.com"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fileNamesArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = fileNamesArray[indexPath.row]
        cell.imageView?.image = UIImage(named: "icon\(indexPath.row)");
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedFile = fileNamesArray[indexPath.row]
        showEmail(attachmentFile: selectedFile)
    }
    
    //MARK:- Mail functions
    
    func showEmail(attachmentFile: String) {
        guard MFMailComposeViewController.canSendMail() else {
            print("This device doesn't allow you to send mail.")
            return
        }
        
        let mailComposer = MFMailComposeViewController()
        mailComposer.mailComposeDelegate = self
        mailComposer.setSubject(emailTitleString)
        mailComposer.setMessageBody(messageBodyString, isHTML: false)
        mailComposer.setToRecipients(toRecipientsArray)
        
        let filePartsArray = attachmentFile.components(separatedBy: ".")
        let fileNameString = filePartsArray[0]
        let fileExtensionString = filePartsArray[1]
        
        guard let filePath = Bundle.main.path(forResource: fileNameString, ofType: fileExtensionString) else {
            return
        }
        if let fileData = try? Data(contentsOf: URL(fileURLWithPath: filePath)),
            let mimeType = MIMEType(type: fileExtensionString) {
            mailComposer.addAttachmentData(fileData, mimeType: mimeType.rawValue, fileName: fileNameString)
            present(mailComposer, animated: true, completion: nil)
        }
    }
}

extension AttachmentTableViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result {
        case MFMailComposeResult.cancelled:
            print("Mail cancelled")
        case MFMailComposeResult.saved:
            print("Mail saved")
        case MFMailComposeResult.sent:
            print("Mail sent")
        case MFMailComposeResult.failed:
            print("Failed to send: \(error?.localizedDescription ?? "")")
        @unknown default:
            fatalError()
        }
        dismiss(animated: true, completion: nil)
    }
}




